package pageObjectModel.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;
import pageObjectModel.page.objects.LandingPage;
import pageObjectModel.page.objects.MainMiddleSectionPage;

import static org.testng.Assert.assertEquals;

public class ShoppingCartTest extends TestBase {

    @Severity(SeverityLevel.NORMAL)
    @Test
    @Description("The goal of this test checks, if we can, to sign in as a not existing user.")
    public void asNotLoggedInUserIShallNotProceedToCheckout() {

        LandingPage landingPage = new LandingPage();
        landingPage.clickOnEnterStoreLink();

        MainMiddleSectionPage mainMiddleSectionPage = new MainMiddleSectionPage();
        String singOnWarningMessage = mainMiddleSectionPage
                .fishLinkClick()
                .angelfishClick()
                .smallAnglefishAddToCart()
                .clickProceedToCheckoutButton()
                .getSingOnWarningMessage();

        assertEquals("You must sign on before attempting to check out. Please sign on and try checking out again.", singOnWarningMessage);
    }
}
