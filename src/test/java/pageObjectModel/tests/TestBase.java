package pageObjectModel.tests;

import io.qameta.allure.Step;
import org.testng.annotations.*;
import pageObjectModel.configuration.ConfigurationProperties;
import pageObjectModel.configuration.PropertiesLoader;
import pageObjectModel.driver.BrowserType;
import pageObjectModel.driver.manager.DriverManager;
import pageObjectModel.driver.manager.DriverUtils;

import java.util.Properties;

import static pageObjectModel.navigation.ApplicationURLs.APPLICATION_URL;

public class TestBase {

    @Step("Loading configuration from configuration.properties")
    @BeforeClass
    public static void beforeClass() {
        PropertiesLoader propertiesLoader = new PropertiesLoader();
        Properties propertiesFromFile = propertiesLoader.getPropertiesFromFile("configuration.properties");
        ConfigurationProperties.setProperties(propertiesFromFile);
    }

    @Step("Setting up browser to: {browserType} and navigating to Home Page")
    @Parameters("browser")
    @BeforeMethod
    public void setup(@Optional BrowserType browserType) {
        DriverManager.setWebDriver(browserType);
        DriverManager.getWebDriver();
        DriverUtils.navigateToPage(APPLICATION_URL);
        DriverUtils.setInitialConfiguration();
    }

    @Step("Disposing browser")
    @AfterMethod
    public void tearDown() {
        DriverManager.disposeDriver();
    }
}
