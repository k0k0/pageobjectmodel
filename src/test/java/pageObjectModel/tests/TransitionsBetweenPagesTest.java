package pageObjectModel.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import org.testng.annotations.Test;
import pageObjectModel.driver.manager.DriverUtils;
import pageObjectModel.page.objects.LandingPage;
import pageObjectModel.page.objects.TopMenuPage;

import static org.testng.Assert.assertTrue;
import static pageObjectModel.navigation.ApplicationURLs.APPLICATION_URL;

public class TransitionsBetweenPagesTest extends TestBase{

    @Severity(SeverityLevel.CRITICAL)
    @Test
    @Description("The goal of this test is checking if the transition between pages work correctly")
    @Step("Checking transitions between pages")
    public void transitionFromLandingPageToLoginPage() {
        DriverUtils.navigateToPage(APPLICATION_URL);

        LandingPage landingPage = new LandingPage();
        boolean islogoIsDisplayed = landingPage
                .clickOnEnterStoreLink()
                .storeLogoIsDisplayed();

        TopMenuPage topMenuPage = new TopMenuPage();
        boolean isLoginButtonDisplayed = topMenuPage
                .clickOnSignInLink()
                .loginButtonIsDisplayed();

        assertTrue(islogoIsDisplayed);
        assertTrue(isLoginButtonDisplayed);
    }

}
