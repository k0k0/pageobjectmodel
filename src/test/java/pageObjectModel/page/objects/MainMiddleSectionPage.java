package pageObjectModel.page.objects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjectModel.waits.WaitForElement;

public class MainMiddleSectionPage extends BasePage {

    @FindBy(css = "area[alt='Fish']")
    WebElement fishLink;

    @Step("Click on the fish icon on the main screen")
    public FishPage fishLinkClick() {
        WaitForElement.waitUntilElementIsClickable(fishLink);
        fishLink.click();
        log().info("Clicked on fish icon button.");
        return new FishPage();
    }
}
