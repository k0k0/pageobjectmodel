package pageObjectModel.page.objects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjectModel.waits.WaitForElement;

public class FishPage extends BasePage{

    @FindBy(xpath = "//a[contains(text(),'FI-SW-01')]")
    WebElement anglefish;

    @Step("Click on the Angel Fish button")
    public AnglefishCartPage angelfishClick() {
        WaitForElement.waitUntilElementIsClickable(anglefish);
        anglefish.click();
        log().info("Clicked on Angelfish button");
        return new AnglefishCartPage();
    }
}
