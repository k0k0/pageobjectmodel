package pageObjectModel.page.objects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjectModel.waits.WaitForElement;

public class ShoppingCartPage extends BasePage{

    @FindBy(xpath = "//a[contains(text(),'Proceed to Checkout')]")
    WebElement proceedToCheckoutButton;

    @Step("Click protect to checkout on the cart screen")
    public LoginPage clickProceedToCheckoutButton() {
        WaitForElement.waitUntilElementIsClickable(proceedToCheckoutButton);
        proceedToCheckoutButton.click();
        log().info("Clicked on Proceed to Checkout button on the Shopping Cart page");
        return new LoginPage();
    }
}
