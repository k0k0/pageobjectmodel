package pageObjectModel.page.objects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjectModel.waits.WaitForElement;

import java.util.List;

public class AnglefishCartPage extends BasePage{

    @FindBy(xpath = "//a[@class='Button']")
    List<WebElement> anglefishAddCartButtons;

    @Step("Add a small Angle Fish to the cart")
    public ShoppingCartPage smallAnglefishAddToCart() {
        WaitForElement.waitUntilElementIsClickable(anglefishAddCartButtons.get(1));
        anglefishAddCartButtons.get(1).click();
        log().info("Clicked small angelfish add to cart button");
        return new ShoppingCartPage();
    }

}
