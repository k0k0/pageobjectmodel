package pageObjectModel.page.objects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjectModel.waits.WaitForElement;

public class TopMenuPage extends BasePage {

    @FindBy(css = "#MenuContent a[href*='signonForm']")
    private WebElement signOnLink;

    @FindBy(id = "LogoContent")
    private WebElement logo;

    @Step("Click on Sign In Link")
    public LoginPage clickOnSignInLink(){
        WaitForElement.waitUntilElementIsClickable(signOnLink);
        signOnLink.click();
        log().info("Clicked on Sing on Link");
        return new LoginPage();
    }

    public boolean storeLogoIsDisplayed() {
        WaitForElement.waitUntilElementsVisible(logo);
        return logo.isDisplayed();
    }
}
