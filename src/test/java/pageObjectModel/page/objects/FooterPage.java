package pageObjectModel.page.objects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjectModel.waits.WaitForElement;

public class FooterPage extends BasePage{

    @FindBy(css = "#Banner img[src*='dog']")
    private WebElement banner;

    @Step("Getting is dog banner is displayed")
    public boolean getBannerAfterLoginLogo() {
        WaitForElement.waitUntilElementsVisible(banner);
        boolean isDisplayed = banner.isDisplayed();
        log().info("Returning status of banner after login: {}", isDisplayed);
        return isDisplayed;
    }
}
