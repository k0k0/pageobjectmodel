package pageObjectModel.page.objects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjectModel.waits.WaitForElement;

public class LoginPage extends BasePage {

    @FindBy(name = "username")
    private WebElement usernameField;

    @FindBy(name = "password")
    private WebElement passwordField;

    @FindBy(name = "signon")
    private WebElement signOnButton;

    @FindBy(css = "#Content ul[class='messages'] li")
    private WebElement warningMessage;

    @FindBy(xpath = "//li[contains(text(), 'You must sign on before')]")
    private WebElement singOnMessage;

    @Step("Type into User Name Field {userName}")
    public LoginPage typeIntoUserNameField(String userName) {
        WaitForElement.waitUntilElementsVisible(usernameField);
        usernameField.clear();
        usernameField.sendKeys(userName);
        log().info("Typed into User Name field: {}", userName);
        return this;
    }

    @Step("Type into Password Field {password}")
    public LoginPage typeIntoPasswordField(String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
        log().info("Typed into Password field: {}", password);
        return this;
    }

    @Step("Click on Login Button")
    public FooterPage clickOnLoginButton() {
        signOnButton.click();
        log().info("Clicked on Login Button");
        return new FooterPage();
    }

    @Step("Getting warning message from Login Page")
    public String getWarningMessage() {
        WaitForElement.waitUntilElementsVisible(warningMessage);
        String warningText = warningMessage.getText();
        log().info("Returned warning message was: {}", warningText);
        return warningText;
    }

    public String getSingOnWarningMessage() {
        WaitForElement.waitUntilElementsVisible(singOnMessage);
        String singOnWarningMessage = singOnMessage.getText();
        log().info("Returned sing on warning message was: {}", singOnWarningMessage);
        return singOnWarningMessage;
    }

    public boolean loginButtonIsDisplayed() {
        WaitForElement.waitUntilElementsVisible(signOnButton);
        return signOnButton.isDisplayed();
    }
}
