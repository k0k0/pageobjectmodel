package pageObjectModel.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {

    private Logger logger = LogManager.getLogger(PropertiesLoader.class);

    //There is only one method in a class. It is used to read properties from the given file
    public Properties getPropertiesFromFile(String propertiesFileName) {

        //We create an InputStream object that will be used to read the properties file
        InputStream inputStream = null;

        //The Properties object will hold the properties
        Properties properties = new Properties();
        try {
            logger.info("Trying to load properties with file name: " + propertiesFileName);

            //We read the properties file and initialize the inputStream object
            inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);

            //If the properties file does not exist, the inputStream object will be null.
            //Therefore, a FileNotFoundException exception will be thrown
            if (inputStream != null) {
                //We load properties in the form of InputStream to the appropriate Properties object
                properties.load(inputStream);
                logger.info("Successfully loaded properties for file: " + propertiesFileName);
            } else {
                throw new FileNotFoundException("Property file '" + propertiesFileName + "' not found in the classpath");
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot load properties due to IOException!");
        } finally {
            //Finally, we close InputStream by calling the private closeResource method
            closeResource(inputStream);
        }

        //We return the initialized properties object
        return properties;
    }

    private void closeResource(InputStream inputStream) {
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
