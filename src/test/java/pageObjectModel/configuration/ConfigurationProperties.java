package pageObjectModel.configuration;

import java.util.Properties;

public class ConfigurationProperties {

    //A static field for storing properties
    private static Properties properties;

    //Private constructor needed to have only one ConfigurationProperties instance
    //Singleton pattern
    private ConfigurationProperties() {
    }

    //The method is used to load the properties object into a static Properties object, available to every thread
    public static void setProperties(Properties properties) {
        ConfigurationProperties.properties = properties;
    }

    //The method returns to us all loaded properties if they are not null
    public static Properties getProperties() {
        if (properties == null) {
            throw new IllegalStateException("Please set properties using setProperties() before calling getProperties()");
        }
        return properties;
    }

}
